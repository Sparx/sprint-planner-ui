![Sprint planner logo](https://gitlab.com/Sparx/sprint-planner-ui/raw/master/client/src/assets/sprint_man.png "Mr Sprint")

# Sprint planner
A Vue.js powered web application to aid distributed sprint teams during planning and pointing of word. Once logged in,
all team members will be able to select a point value for the current story being talked about and once everyone has voted,
the scrum master will have the ability to reveal the estimates to the rest of the team for further discussion.

## Functionality
### Scrum Teams
By providing a case-sensitive name on the landing page, you will create a room for the rest of your scrum team to join. This
provides some isolation and allows numerous teams to carry out planning at the same time.

### Desktop Notifications
If allowed, the app will display various desktop notifications for certain events over the course of planning.

Notifications supported:
- Story changing by scrum master
- Nudging of inactive users
- Votes displayed

## Supported Browsers
- Chrome 38+
- Firefox 42+
- Safari 9+
- Opera 25+
- Edge 12+

## Possible Future Functionality
### Password protected scrum teams
There may be times when open access simply by knowing the team name isn't desired so the ability to add a password or some form
of access token when joing would be desirable.

### Agile software integration
A distant future feature, but integration into services like JIRA could allow the app to automatically add the chosen story
point value to the accompanying story in the agile software/service of choice.

## Back-end
The accompanying API can be found [here](https://gitlab.com/Sparx/sprint-planner-api). This is a very bare-bones implemention of the back-end that should
allow you to get basic functionality up and running but it currently lacks more advanced features like being able to support multiple teams planning at once and password protected planning sessions.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run all tests
npm test
```

## Credits
[Vue-Socket.io](https://github.com/MetinSeylan/Vue-Socket.io) - Base implementation of Socket.io inside a Vue plugin
