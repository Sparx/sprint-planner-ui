// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueResource from 'vue-resource';
import Element from 'element-ui';
import VueChartkick from 'vue-chartkick';
import Chart from 'chart.js';
import App from './App';
import router from './router';
import store from './store';

import './assets/element-variables.scss';
import './assets/app.scss';
import './assets/element-overrides.scss';

Vue.config.productionTip = false;

Vue.use(VueChartkick, {
  adapter: Chart
});
Vue.use(VueResource);
Vue.use(Element);
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
});
