import socket from 'socket.io-client';
import Emitter from './vueSocketEmitter';

export default class {
  constructor(connection, store) {
    if (typeof connection === 'string') {
      this.Socket = socket(connection);
    } else {
      this.Socket = connection;
    }
    if (store) {
      this.store = store;
    }
    this.onEvent();
  }
  onEvent() {
    const self = this;
    const superOnEvent = self.Socket.onevent;
    this.Socket.onevent = (packet) => {
      superOnEvent.call(self.Socket, packet);
      Emitter.emit(packet.data[0], packet.data[1]);
      if (self.store) {
        self.passToStore(`SOCKET_${packet.data[0]}`, [...packet.data.slice(1)]);
      }
    };
    ['connect', 'error', 'disconnect', 'reconnect', 'reconnect_attempt', 'reconnecting', 'reconnect_error', 'reconnect_failed', 'connect_error', 'connect_timeout', 'connecting', 'ping', 'pong']
      .forEach((value) => {
        self.Socket.on(value, (data) => {
          Emitter.emit(value, data);
        });
      });
  }
  passToStore(event, payload) {
    if (!event.startsWith('SOCKET_')) {
      return;
    }
    // eslint-disable-next-line no-underscore-dangle, guard-for-in, no-restricted-syntax
    for (const namespaced in this.store._mutations) {
      const mutation = namespaced.split('/').pop();
      if (mutation.toUpperCase() === event.toUpperCase()) {
        this.store.commit(namespaced, payload);
      }
    }
    // eslint-disable-next-line no-underscore-dangle, guard-for-in, no-restricted-syntax
    for (const namespaced in this.store._actions) {
      const action = namespaced.split('/').pop();
      if (action.startsWith('socket_')) {
        const camelcased = `socket_${event}`
          .replace('SOCKET_', '')
          .replace(/^([A-Z])|[\W\s_]+(\w)/g, (match, p1, p2) => {
            if (p2) {
              return p2.toLowerCase();
            }
            return p1.toUpperCase();
          });

        if (action === camelcased) {
          this.store.dispatch(namespaced, payload);
        }
      }
    }
  }
}
