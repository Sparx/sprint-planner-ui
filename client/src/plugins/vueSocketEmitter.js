// eslint-disable-next-line new-parens
export default new class {
  constructor() {
    this.listeners = new Map();
  }
  addListener(label, cb, vm) {
    if (typeof cb === 'function') {
      // eslint-disable-next-line no-unused-expressions
      this.listeners.has(label) || this.listeners.set(label, []);
      this.listeners.get(label).push({ callback: cb, vm });
      return true;
    }
    return false;
  }
  removeListener(label, cb, vm) {
    const listeners = this.listeners.get(label);
    let index;
    if (listeners && listeners.length) {
      index = listeners.reduce((i, listener, currentIndex) => {
        let counter = i;
        const retVal = (typeof listener === 'function' && listener.callback === cb && listener.vm === vm) ?
          counter = currentIndex :
          counter;
        return retVal;
      }, -1);
      if (index > -1) {
        listeners.splice(index, 1);
        this.listeners.set(label, listeners);
        return true;
      }
    }
    return false;
  }
  emit(label, ...args) {
    const listeners = this.listeners.get(label);
    if (listeners && listeners.length) {
      listeners.forEach((listener) => {
        listener.callback.call(listener.vm, ...args);
      });
      return true;
    }
    return false;
  }
};
