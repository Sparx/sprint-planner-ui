module.exports = {
  appBaseUrl: 'http://localhost:8080',
  routeBaseUrl: '/',
  apiUrl: 'http://localhost:9000/api',
  socket: {
    url: 'http://localhost:9000',
    path: '/socket'
  }
};
