module.exports = {
  appBaseUrl: 'https://onesimplecoder.com/codeshed/planner',
  routeBaseUrl: '/codeshed/planner',
  apiUrl: 'https://onesimplecoder.com/codeshed/planner/api',
  socket: {
    url: 'https://onesimplecoder.com',
    path: '/codeshed/planner/socket'
  }
};
