/* eslint-disable global-require */
if (process.env.NODE_ENV === 'production') {
  module.exports = require('./appConfig.prod.js');
} else {
  module.exports = require('./appConfig.dev.js');
}
