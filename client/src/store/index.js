/* eslint-disable no-param-reassign */
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

const baseState = {
  user: {},
  loggedInUsers: [],
  currentStory: '',
  showVotes: false,
  votes: [],
  voted: false,
  scrumMaster: false,
  joined: false,
  myVote: 0
};
function getBaseState() {
  return baseState;
}
export const mutations = {
  clearVotes(state) {
    state.votes = [];
    state.showVotes = false;
    state.voted = false;
    state.myVote = 0;
  },
  endPlanning(state) {
    state.user = {};
    state.loggedInUsers = [];
    state.currentStory = '';
    state.votes = [];
    state.voted = false;
    state.showVotes = false;
    state.scrumMaster = false;
    state.joined = false;
    state.myVote = 0;
  },
  resetVote(state) {
    state.myVote = 0;
    state.voted = false;
  },
  setStory(state, story) {
    state.currentStory = story;
  },
  setUser(state, user) {
    state.user = user;
  },
  showVotes(state) {
    state.showVotes = true;
  },
  updateVote(state, vote) {
    state.myVote = vote;
  },
  voted(state) {
    state.voted = true;
  },
  SOCKET_addVote(state, vote) {
    state.votes.push(vote[0]);
  },
  SOCKET_clearVotes(state) {
    state.votes = [];
    state.showVotes = false;
    state.voted = false;
    state.myVote = 0;
  },
  SOCKET_endPlanning(state) {
    state.user = {};
    state.loggedInUsers = [];
    state.currentStory = '';
    state.votes = [];
    state.voted = false;
    state.showVotes = false;
    state.scrumMaster = false;
    state.joined = false;
    state.myVote = 0;
  },
  SOCKET_initialPlanningState(state, socketResponse) {
    const initialState = socketResponse[0];
    state.loggedInUsers = initialState.users;
    state.votes = initialState.votes;
    state.currentStory = initialState.currentStory;
    state.joined = true;
  },
  SOCKET_resetUserVote(state, userId) {
    const id = userId[0];
    state.votes = state.votes.filter(v => v.userId !== id);
  },
  SOCKET_setStory(state, story) {
    state.currentStory = story[0];
  },
  SOCKET_showVotes(state) {
    state.showVotes = true;
  },
  SOCKET_userArrived(state, user) {
    // Events from the socket plugin will have the data in array format
    state.loggedInUsers.push(user[0]);
  },
  SOCKET_userLeft(state, user) {
    const newUserList = state.loggedInUsers.filter(u => u.id !== user[0].id);
    const newVoteList = state.votes.filter(v => v.userId !== user[0].id);
    state.loggedInUsers = newUserList;
    state.votes = newVoteList;
  }
};

export default new Vuex.Store({
  state: getBaseState(),
  mutations,
  strict: process.env.NODE_ENV !== 'production'
});
