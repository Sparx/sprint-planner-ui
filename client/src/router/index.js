import Vue from 'vue';
import Router from 'vue-router';

import Landing from '@/components/Landing';
import Planner from '@/components/planner/PlannerBase';
import Error from '@/components/Error';
import NotFound from '@/components/NotFound';

import store from '../store';
import appConfig from '../appConfig';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: appConfig.routeBaseUrl,
  routes: [
    {
      path: '/',
      name: 'Landing',
      component: Landing
    },
    {
      path: '/team',
      name: 'Planner',
      component: Planner,
      beforeEnter: (to, from, next) => {
        if (!store.state.user.name) {
          next({
            path: '/'
          });
        } else {
          next();
        }
      }
    },
    {
      path: '/:teamId',
      name: 'LandingWithTeamId',
      component: Landing,
      props: route => ({
        teamId: route.params.teamId
      })
    },
    {
      path: '/error',
      name: 'Error',
      component: Error,
      props: route => ({
        code: route.query.code
      })
    },
    {
      path: '*',
      name: 'NotFound',
      component: NotFound
    }
  ]
});
