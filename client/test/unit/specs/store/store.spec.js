/* eslint-disable no-unused-expressions */
import { expect } from 'chai';
import { mutations } from '../../../../src/store';

const { clearVotes,
  endPlanning,
  resetVote,
  setStory,
  setUser,
  showVotes,
  updateVote,
  voted,
  SOCKET_initialPlanningState,
  SOCKET_setStory,
  SOCKET_userArrived,
  SOCKET_userLeft,
  SOCKET_showVotes,
  SOCKET_addVote,
  SOCKET_clearVotes,
  SOCKET_endPlanning,
  SOCKET_resetUserVote } = mutations;

describe('Vuex store', () => {
  describe('mutations', () => {
    it('clearVotes', () => {
      const mockState = {
        votes: [1, 2, 3, 5, 8],
        voted: true,
        showVotes: true,
        myVote: 8
      };
      clearVotes(mockState);
      expect(mockState.votes.length).to.equal(0);
      expect(mockState.showVotes).to.be.false;
      expect(mockState.voted).to.be.false;
      expect(mockState.myVote).to.equal(0);
    });
    it('endPlanning', () => {
      const mockState = {
        users: [
          { name: 'Jeff Bridges', id: 1 },
          { name: 'Noel Edmonds', id: 2 }
        ],
        votes: [
          { vote: 13, userId: 1 }
        ],
        currentStory: 'Jeff Bridges Upbringing',
        myVote: 8
      };
      endPlanning(mockState);
      expect(mockState.loggedInUsers.length).to.equal(0);
      expect(mockState.votes.length).to.equal(0);
      expect(mockState.currentStory).to.equal('');
      expect(mockState.myVote).to.equal(0);
    });
    it('resetVote', () => {
      const mockState = {
        myVote: 5,
        voted: true
      };
      resetVote(mockState);
      expect(mockState.voted).to.be.false;
      expect(mockState.myVote).to.equal(0);
    });
    it('setStory', () => {
      const mockState = {
        currentStory: ''
      };
      setStory(mockState, 'Test Story');
      expect(mockState.currentStory).to.equal('Test Story');
    });
    it('setUser', () => {
      const mockState = {
        name: '',
        team: '',
        id: ''
      };
      setUser(mockState, { name: 'Jeff Bridges', id: '1', team: 'TEST' });
      expect(mockState.user.name).to.equal('Jeff Bridges');
      expect(mockState.user.team).to.equal('TEST');
      expect(mockState.user.id).to.equal('1');
    });
    it('showVotes', () => {
      const mockState = {
        showVotes: false
      };
      showVotes(mockState, true);
      expect(mockState.showVotes).to.be.true;
    });
    it('updateVote', () => {
      const mockState = {
        myVote: 0
      };
      updateVote(mockState, 10);
      expect(mockState.myVote).to.equal(10);
    });
    it('voted', () => {
      const mockState = {
        voted: false
      };
      voted(mockState);
      expect(mockState.voted).to.be.true;
    });
    it('SOCKET_initialPlanningState', () => {
      const mockState = {
        loggedInUsers: [],
        votes: [],
        currentStory: '',
        joined: false
      };
      SOCKET_initialPlanningState(mockState, [
        {
          users: [
            { name: 'Jeff Bridges', id: 1 },
            { name: 'Noel Edmonds', id: 2 }
          ],
          votes: [
            { vote: 13, userId: 1 }
          ],
          currentStory: 'Jeff Bridges Upbringing'
        }
      ]);
      expect(mockState.loggedInUsers.length).to.equal(2);
      expect(mockState.loggedInUsers[0].name).to.equal('Jeff Bridges');
      expect(mockState.votes.length).to.equal(1);
      expect(mockState.votes[0].vote).to.equal(13);
      expect(mockState.currentStory).to.equal('Jeff Bridges Upbringing');
      expect(mockState.joined).to.be.true;
    });
    it('SOCKET_resetUserVote', () => {
      const mockState = {
        votes: [
          { vote: 13, userId: 1 },
          { vote: 5, userId: 2 }
        ]
      };
      SOCKET_resetUserVote(mockState, [
        2
      ]);
      expect(mockState.votes.length).to.equal(1);
    });
    it('SOCKET_setStory', () => {
      const mockState = {
        currentStory: ''
      };
      SOCKET_setStory(mockState, [
        'JIRA-1'
      ]);
      expect(mockState.currentStory).to.equal('JIRA-1');
    });
    it('SOCKET_userArrived', () => {
      const mockState = {
        loggedInUsers: []
      };
      SOCKET_userArrived(mockState, [
        { name: 'Jeff Bridges', id: 1 }
      ]);
      expect(mockState.loggedInUsers[0].name).to.equal('Jeff Bridges');
    });
    it('SOCKET_userLeft', () => {
      const mockState = {
        loggedInUsers: [
          { name: 'Jeff Bridges', id: 1 },
          { name: 'Mr Frodo', id: 2 }
        ],
        votes: [
          { vote: 13, userId: 1 }
        ]
      };
      SOCKET_userLeft(mockState, [
        { name: 'Jeff Bridges', id: 1 }
      ]);
      expect(mockState.loggedInUsers[0].name).to.equal('Mr Frodo');
      expect(mockState.votes.length).to.equal(0);
    });
    it('SOCKET_showVotes', () => {
      const mockState = {
        showVotes: false
      };
      SOCKET_showVotes(mockState, true);
      expect(mockState.showVotes).to.be.true;
    });
    it('SOCKET_addVote', () => {
      const mockState = {
        votes: []
      };
      SOCKET_addVote(mockState, [
        { userId: 1, vote: 13 }
      ]);
      expect(mockState.votes[0].userId).to.equal(1);
      expect(mockState.votes[0].vote).to.equal(13);
    });
    it('SOCKET_clearVotes', () => {
      const mockState = {
        votes: [1, 2, 3, 5, 8],
        voted: true,
        showVotes: true,
        myVote: 8
      };
      SOCKET_clearVotes(mockState);
      expect(mockState.votes.length).to.equal(0);
      expect(mockState.showVotes).to.be.false;
      expect(mockState.voted).to.be.false;
      expect(mockState.myVote).to.equal(0);
    });
    it('SOCKET_endPlanning', () => {
      const mockState = {
        users: [
          { name: 'Jeff Bridges', id: 1 },
          { name: 'Noel Edmonds', id: 2 }
        ],
        votes: [
          { vote: 13, userId: 1 }
        ],
        currentStory: 'Jeff Bridges Upbringing',
        myVote: 8
      };
      SOCKET_endPlanning(mockState);
      expect(mockState.loggedInUsers.length).to.equal(0);
      expect(mockState.votes.length).to.equal(0);
      expect(mockState.currentStory).to.equal('');
      expect(mockState.myVote).to.equal(0);
    });
  });
});
