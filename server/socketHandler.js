'use strict';
class SocketHandler {
    constructor(db) {
        this.db = db;
    }

    clearVotes(team) {
        this.db.clearVotes(team);
    }
    endPlanning(team) {
        this.db.endPlanning(team);
    }
    getPlanningState(team) {
        return this.db.getPlanningState(team);
    }
    kickUser(team, userId) {
        this.db.kickUser(team, userId);
    }
    resetUserVote(team, userId) {
        this.db.resetUserVote(team, userId);
    }
    setStory(team, story) {
        this.db.setStory(team, story);
    }
    userArrived(team, user) {
        this.db.userArrived(team, user);
    }
    userLeft(team, user) {
        this.db.userLeft(team, user);
    }
    userVoted(team, vote) {
        this.db.userVoted(team, vote);
    }
}

module.exports = SocketHandler;