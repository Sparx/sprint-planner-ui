'use strict';
class MemoryDb {
    get teams() {
        return this.db.teams;
    }

    constructor() {
        this.db = {
            teams:[]
        };
    }

    clearVotes(teamId) {
        const team = this.db.teams.find(t => t.id === teamId);
        if (team === undefined)
            return undefined;

        team.votes = [];
    }

    createTeam(user) {
        this.db.teams.push({
            id: user.team,
            users: [],
            votes: [],
            currentStory: ''
        });
    }

    endPlanning(teamId) {
        const team = this.db.teams.find(t => t.id === teamId);
        if (team === undefined)
            return undefined;

        this.db.teams = this.db.teams.filter(t => t.id !== teamId);
    }

    kickUser(teamId, userId) {
        const team = this.db.teams.find(t => t.id === teamId);
        if(team === undefined)
            return undefined;

        team.users = team.users.filter(u => u.id !== userId);
    }

    resetUserVote(teamId, userId) {
        const team = this.db.teams.find(t => t.id === teamId);
        if(team === undefined)
            return undefined;

        team.votes = team.votes.filter(v => v.userId !== userId);
    }

    setStory(teamId, story) {
        const team = this.db.teams.find(t => t.id === teamId);
        if (team === undefined)
            return undefined;

        team.currentStory = story;
    }

    userArrived(teamId, user) {
        const team = this.db.teams.find(t => t.id === teamId);
        if (team === undefined)
            return undefined;

        team.users.push(user);
    }

    userLeft(teamId, user) {
        const team = this.db.teams.find(t => t.id === teamId);
        if (team === undefined)
            return undefined;

        team.users = team.users.filter(u => u.id !== user.id);
        team.votes = team.votes.filter(v => v.userId !== user.id);

        if(team.users.length === 0) {
            console.log(`Removing empty team from DB. TeamId:${teamId}.`);
            this.db.teams.filter(t => t.id !== teamId);
        }
    }

    userVoted(teamId, vote) {
        const team = this.db.teams.find(t => t.id === teamId);
        if (team === undefined)
            return undefined;

        team.votes.push(vote);
    }
}

module.exports = MemoryDb;