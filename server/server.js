'use strict';
const http = require('http');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const SocketHandler = require('./socketHandler');
const MemoryDb = require('./db/memoryDb');

const port = 9000;

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

if(process.env.NODE_ENV !== 'production') {
    const corsOptions= {
        origin: 'http://localhost:8080',
        optionsSuccessStatus: 200
    };
    app.use(cors(corsOptions));
}

const server = http.Server(app);
const io = require('socket.io')(server, {
    path: '/socket',
    serveClient: false
});

server.listen(port, err => {
    if(err) {
        console.error(err);
    } else {
        console.log(`Server listening on ${port}...`);
    }
});

const db = new MemoryDb();
const socketHandler = new SocketHandler(db);

app.post('/api/user/join', (req, res) => {
    if(!req.body.name) {
        res.status(400).send('Please provide your name');
    }

    if(!req.body.team) {
        res.status(400).send('Please provide the name of your scrum team');
    }

    const team = db.teams.find(t => t.id === req.body.team);
    if(!team) {
        db.createTeam(req.body);
    }

    res.status(200).send();
});

io.on('connection', socket => {
    socket.join(decodeURIComponent(socket.handshake.query.team));

    socket.on('clearVotes', team => {
        console.log(`Event: clearVotes. Team:${team}`);
        socketHandler.clearVotes(team);
        socket.to(team).emit('clearVotes');
    });
    socket.on('endPlanning', team => {
        console.log(`Event: endPlanning. Team:${team}`);
        socketHandler.endPlanning(team);
        socket.to(team).emit('endPlanning');
    });
    socket.on('getPlanningState', team => {
        console.log(`Event: getPlanningState. Team:${team}`);
        const state = socketHandler.getPlanningState(team);
        socket.emit('initialPlanningState', state);
    });
    socket.on('kickUser', payload => {
       console.log(`Event: kickUser. UserId:${payload.userId}. Team:${payload.team}`);
       socketHandler.kickUser(payload.team, payload.userId);
       socket.to(payload.team).emit('userKicked', payload.userId);
    });
    socket.on('nudgeUser', payload => {
        console.log(`Event: nudgeUser. UserId:${payload.userId}. Team:${payload.team}`);
        socket.to(payload.team).emit('nudgeUser', payload.userId);
    });
    socket.on('resetVote', payload => {
       console.log(`Event: resetVote. UserId:${payload.userId}. Team:${payload.team}`);
       socketHandler.resetUserVote(payload.team, payload.userId);
       io.in(payload.team).emit('resetUserVote', payload.userId);
    });
    socket.on('setStory', payload => {
        console.log(`Event: setStory. Story:${payload.story}. Team:${payload.team}`);
        socketHandler.setStory(payload.team, payload.story);
        socket.to(payload.team).emit('setStory', payload.story);
    });
    socket.on('showVotes', team => {
        console.log(`Event: showVotes. Team:${team}`);
        socket.to(team).emit('showVotes');
    });
    socket.on('userArrived', payload => {
        console.log(`Event: userArrived. User:${JSON.stringify(payload.user)}. Team:${payload.team}`);
        socketHandler.userArrived(payload.team, payload.user);
        socket.to(payload.team).emit('userArrived', payload.user);
        const team = db.teams.find(t => t.id === payload.team);
        socket.emit('initialPlanningState', team);
    });
    socket.on('userLeft', payload => {
        console.log(`Event: userLeft. User:${JSON.stringify(payload.user)}. Team:${payload.team}`);
        socketHandler.userLeft(payload.team, payload.user);
        socket.to(payload.team).emit('userLeft', payload.user);
    });
    socket.on('userVoted', payload => {
        console.log(`Event: userVoted. Vote:${JSON.stringify(payload.vote)}. Team:${payload.team}`);
        socketHandler.userVoted(payload.team, payload.vote);
        socket.to(payload.team).emit('addVote', payload.vote);
    });
});

